﻿using ORM.Dapper.Context;
using ORM.Dapper.Entities;
using ORM.Dapper.Repositories;

namespace ORM.Dapper.ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            const string connectionString = "Data Source=(localdb)\\ProjectModels;Initial Catalog=ShopDB;Integrated Security=True";
            var context = new ShopDbContext(connectionString);

            var productRepo = new ProductRepository(context);

            //var products = productRepo.GetAllProducts();

            //foreach (var product in products)
            //{
            //    Console.WriteLine(product.Name);
            //}



            var orderRepo = new OrderRepository(context);

            //var orderEntity = new Order()
            //{
            //    CreatedDate = DateTime.Now,
            //    ProductId = 4,
            //    Status = OrderStatus.Loading,
            //    UpdateDate = DateTime.Now + TimeSpan.FromHours(5)
            //};

            //orderRepo.Create(orderEntity);



            //var orders = orderRepo.GetAll(productId: 4, status: OrderStatus.Loading);

            //foreach (var order in orders)
            //{
            //    Console.WriteLine(order.Id);
            //}
        }
    }
}

